#include "DTC_new.h"




/*==============================================================================
						methodes
==============================================================================*/

//  Digital Fourier Transform implementation
void CalculationDFT(float * SampleTab,//tab with sampled function values
					float * MagTab,   //tab with magnitute for each harmonics
					float * PhaseTab) //tab with phase shifts for each harmonics
{
	int k,n;
	float alfa, mux;
	double * AR, * AI;
	
	if ((AR = (double *) malloc(sizeof(double)*N)) == NULL){
		printf("%c",'\x7');}	// Real component
	 
	if ((AI = (double *) malloc(sizeof(double)*N)) == NULL){
		printf("%c",'\x7');} 	// Imaginary component	
	
	for(k=0;k<K;k++){
		AR[k]=0;
		AI[k]=0;
		for(n=0;n<N;n++){// calculations related to magnitude of harmonics
			mux=1.0/N;
			alfa=2.0*PI*k*n/N;
			AR[k]=AR[k]+mux*SampleTab[n]*cos(alfa);
			AI[k]=AI[k]-mux*SampleTab[n]*sin(alfa);
		}
	}
	for(k=0;k<K;k++){// calculations related to phase of harmonics
		if(sqrt(AR[k]*AR[k]) < APPROXIMATION){
	 		if (AI[k]>=0){
				PhaseTab[k]=PI/2.0;
	 		}else{
				PhaseTab[k]=-PI/2.0;
			}      
		}else if (AR[k] > 0){
			PhaseTab[k]=atan(AI[k]/AR[k]);   // phase of each harmonics
		}else{
			PhaseTab[k]=PI+atan(AI[k]/AR[k]);
		} 
		if(k==0){	// magnitude for constant component of the function
			MagTab[k]=sqrt(AI[k]*AI[k]+AR[k]*AR[k]);		
		}else{		// magnitude for each harmonics
			 MagTab[k]=2*sqrt(AI[k]*AI[k]+AR[k]*AR[k]);  
		}	
	}
	free(AR); free(AI);
}
/*============================================================================*/
// Perform Reverse Digital Fourier Transform 
float Reconstruction(float * SampleTab,  // tab with sampled function wave 
					 float * ReconstrTab,// tab with reconstructed wave 
					 float * MagTab,	 // tab with magnitude of harmonics
		    		 float * PhaseTab)	 // tab with phase shifts for each harm.
{
	int n,k;
	// angle to be used to sweep the sin and cos from 0 to 2*Pi
	float alfa;		
	// this tab will keep the component waves e.g. each meaningful harmonics
	float * TempTab;	
	
	if ((TempTab = (float*) malloc(sizeof(float)*N)) == NULL){
		printf("%c",'\x7');
	} 
	
	for(n=0;n<N;n++){
		ReconstrTab[n]=0.0;  // to zero the values in the Reconstruction table
	} 
	for(k=0;k<=(K/2);k++){
		for(n=0;n<N;n++)   		// to zero Temp Table
	 		TempTab[n]=0.0;
	 	for(n=0;n<N;n++){
			alfa=(2.0*PI*n*1.0*k)/N;    //
			ReconstrTab[n]=ReconstrTab[n]+MagTab[k]*cos(alfa+PhaseTab[k]);	
			// Temp subsequent components of the function
			TempTab[n]=TempTab[n]+MagTab[k]*cos(alfa+PhaseTab[k]); 
		}
		WriteOneLineToFile(TempTab);	
	}
	free(TempTab);	
	return 0;
}
/*============================================================================*/
void OpenFile()
{			
	if ((FileHandler=fopen("harm2.txt","w"))==NULL) {
		printf("%c Problem to open file",'\x7');
		exit(-1);
	}
}
/*============================================================================*/
// Write one line with reconstructed data to a file and go to new line
void WriteOneLineToFile(float * TempTab)
{
	int n;
	for(n=0;n<N;n++)
	fprintf(FileHandler,"%.4f;", TempTab[n]);
	
	fprintf(FileHandler,"\n");
}
/*============================================================================*/
void FillTabSin(float *Tab)        // fill table with sampled sin wave data
{
	int i;
	float alfa;
	float *ptr=Tab;
	float delta;
	
	delta=(PI/N)*1/2;
	
	for(i=0;i<N;i=i+1){  
	   alfa=(2*PI*i/N+delta+PI);
	   *ptr=sin(alfa);
	   ptr++;
	}
}
/*============================================================================*/
void FillTabSumSin(float *Tab)
{
	int i;
	float alfa;
	float *ptr=Tab;
	float delta;
	
	delta=(PI/N)*1/2;
	
	for(i=0;i<N;i++){
	   alfa=(2*PI*i/N+delta);
	   *ptr=0.6*sin(alfa);	//+
	   ptr++;
	}
	ptr=Tab;
	
	for(i=0;i<N;i=i+1){
	alfa=(2*PI*i/N+delta);
	alfa=-0.4*sin(2*alfa);  // -
	*ptr+=alfa;
	ptr++;
	}
	ptr=Tab;
	
	for(i=0;i<N;i=i+1){
	   alfa=(2*PI*i/N+delta);
	   alfa=-0.1*sin(11*alfa);    // -
	   *ptr+=alfa;
	   ptr++;
	}
}
/*============================================================================*/
void FillTabDoubleSin(float *Tab)
{
	int i;
	float *ptr=Tab, delta;
	
	delta=(PI/N)*1/2;
	
	for(i=0;i<N;i++){
	   *ptr=abs(sin(2*2*PI*i/N+delta));
	   ptr++;
	}
}
/*============================================================================*/
void FillTabTriangle2(float *Tab)
{
	int i;
	float *ptr=Tab, alfa, delta;
	
	delta=1/2;
	
	*ptr=0;   // Sample No 0
	ptr++;
	
	for (i=0;i<N2Q;i++){
	 	alfa=i+delta;
	 	alfa=(alfa/N2Q);
	 	*ptr=alfa;
	 	ptr++;
	}
	
	for (i=N2Q;i<N;i++){
		alfa=i-N2Q+delta;
	 	alfa=(-1+alfa/N2Q);
	 	*ptr=alfa;
		ptr++;
	}
}
/*============================================================================*/
void FillTabHalfSin(float *Tab)
{
	int i;
	float *ptr=Tab, delta;
	
	delta=(PI/N)*1/2;
	
	for(i=0;i<N1Q;i++){
		*ptr=sin(2*PI*i/N+delta);
	   	ptr++;
	}
	for(i=64;i<N2Q;i++){
		*ptr=0;
		ptr++;
	}
	for(i=N2Q;i<N3Q;i++){
	   *ptr=sin(2*PI*i/N+delta);
	   ptr++;
	}
	for(i=N3Q;i<N;i++){
	   *ptr=0;
	   ptr++;
	}
}
/*============================================================================*/
void FillTabTriangle(float *Tab)    // Tab - pointer to sample table
{
	int i;
	float *ptr=Tab, alfa, delta;
	
	delta=1/2;
	
	for (i=0;i<N1Q;i++){
	 	alfa=i+delta;
	 	*ptr=(alfa/N1Q);
	 	ptr++;
	}
	for(i=N1Q;i<N3Q;i++){
		alfa=i-N1Q+delta;
		*ptr=(1-alfa/N1Q);
		ptr++;
	}
	for(i=N3Q;i<N;i++){
	 	alfa=i-N3Q+delta;
		*ptr=(-1+alfa/N1Q);
	 	ptr++;
	 }
}
/*============================================================================*/
void FillTabRectangle(float *Tab) // Fill table with sampled sqare wave data
{
	int i;
	float *ptr=Tab;
	
	for(i=0;i<N/2;i++){
		*ptr=1;
		ptr++;
	}
	Tab[0]=0; // start with Sample No 0
	
	for(i=N/2;i<N;i++){
		*ptr=-1;
		ptr++;
	}
	i=N/2;
	Tab[i]=0; // start with Sample No N/2
}
/*============================================================================*/
void FreeMe(float *SampleTab, float * MagTab, 
			float * PhaseTab, float * ReconstrTab)
{
  free(SampleTab);
  free(MagTab);
  free(PhaseTab);
  free(ReconstrTab);
}
/*============================================================================*/
