// This program shows the Digital Fourier Transformation method,
// Based on the algorithm from the book Algorithms and Data Structures.
// Samples from time domain are used to perform DCT and generate info on 
// harmonics (magnitude and phase).
// Based on the magnitude and phase of harmonics the wave can be reconstructed.
// To help understand the reconstruction process - data for reconstruction 
// stages are writen to the file.
// Data from the file can be open in Excel and the cumulative graph is created.
// Rectangle wave was chosen for calculations as it has endless spectrum 
// and will show imperfect reconstruction. 
// Even if faster algorithms exist, this one was chosen as easier to understand.


// include section

#include "DTC_new.h"
int main()
{
   	float * SampleTab, * PhaseTab, * MagTab, * ReconstrTab;
	OpenFile();
	// table to keep real values of the function
  	if ((SampleTab = (float *) malloc(sizeof(float)*N)) == NULL)
		printf("%c",'\x7');	 
	//table to keep  phase shift for each harmonics
   	if ((PhaseTab = (float *) malloc(sizeof(float)*N)) == NULL)
		printf("%c",'\x7');  
	//table to keep magnitude of harmonics
   	if ((MagTab = (float *) malloc(sizeof(float)*N)) == NULL)
		printf("%c",'\x7');
	//table to keep values of reconstructed function
  	if ((ReconstrTab = (float *) malloc(sizeof(float)*N)) == NULL) 
		printf("%c",'\x7'); 

	// Fill SampleTable with with sampled wave and use it for DCT calculations
	// FillTabRectangle(SampleTab);
	// FillTabSin(SampleTab);
	// FillTabSumSin(SampleTab);
	// FillTabDoubleSin(SampleTab);
	// FillTabTriangle2(SampleTab);
 	// FillTabHalfSin(SampleTab);
	FillTabTriangle(SampleTab);
 	
	CalculationDFT(SampleTab,MagTab,PhaseTab);
    Reconstruction(SampleTab,ReconstrTab,MagTab,PhaseTab);
	
    FreeMe(SampleTab,MagTab,PhaseTab,ReconstrTab);
	system("pause");
	fclose(FileHandler);		
	return 0;     
}
