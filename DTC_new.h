/* This is what i have to say about my project:
 *
 * "Lorem ipsum dolor sit amet, consectetur adipiscing elit,
 * sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
 * Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
 * nisi ut aliquip ex ea commodo consequat."
 * 
 */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <conio.h>
#include <stdio.h>
#include <string.h> 
 
#ifndef _DTC_new_
#define _DTC_new_

#define PI   	4*atan(1.0)		// PI calculated based on arc tan 45'
#define APPROXIMATION 	0.001	// below this value we assume 0
#define N 		256   			// number of samples, must be 2^n
#define N1Q		64				// 1 quarter
#define N2Q		128				// 2 quarters
#define N3Q		192				// 3 quarters
#define K 		16  			// number of harmonics, must be 2^n < N

FILE *FileHandler; // global file handler to be used in WriteOneLineToFile()

/*==============================================================================
						methodes
==============================================================================*/
//  Digital Fourier Transform implementation
void CalculationDFT(float * SampleTab,//tab with sampled function values
					float * MagTab,   //tab with magnitute for each harmonics
					float * PhaseTab);//tab with phase shifts for each harmonics
					
// Perform Reverse Digital Fourier Transform 
float Reconstruction(float * SampleTab,  // tab with sampled function wave 
					 float * ReconstrTab,// tab with reconstructed wave 
					 float * MagTab,	 // tab with magnitude of harmonics
		    		 float * PhaseTab);	 // tab with phase shifts for each harm.
void OpenFile();
void WriteOneLineToFile(float * TempTab);
void FillTabSin(float *Tab); 		// fill table with sampled sin wave data
void FillTabSumSin(float *Tab);
void FillTabDoubleSin(float *Tab);
void FillTabTriangle2(float *Tab);
void FillTabHalfSin(float *Tab);
void FillTabTriangle(float *Tab);
void FillTabRectangle(float *Tab);// Fill table with sampled sqare wave data
void FreeMe(float *SampleT, float * MagT, float * PhaseT, float * ReconstrT);
#endif //_DTC_new_
